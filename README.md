# DEPRECATED
This project is not maintained anymore.

# Sand

Sand is a JavaScript utility library delivering modularity, consistency and more, following UMD pattern.

## Installation

### Manual download

See my [releases](https://github.com/hpedrorodrigues/Sand/releases) page for download.

### NPM

```bash
npm install --save sand-util
```

## Load

Directly by HTML using global variable **sand**.

```html
<script src="sand.min.js"></script>
```

CommonJS

```javascript
var sand = require('sand-util');
```

AMD

```javascript
require('sand-util', function (sand) {
  ...
});
```

## Usage

Please do your project use the default name for our library like **sand**.

In the sand there are specific methods for certain types.

- [Function](#function)
- [Object](#object)
- [Array](#array)
- [Number](#number)
- [Date](#date)

###### Function

**Throttle** - Throttle calls to *callback* routine and ensure that it is not
invoked any more often than *delay* milliseconds.

```javascript
function method() {
  ...
}

var delay = 100;

sand.throttle(delay, method);
```

**Debounce** - Returns a function, that, as long as it continues to be invoked,
will not be triggered. The function will be called after it stops being called
for *n* milliseconds.

```javascript
function method() {
  ...
}

var delay = 100;

sand.debounce(delay, method);
```

###### Array

**Find** - Find a object in an array according to informed rule.

```javascript
var array = [{a: 2, b: 'b'}, {a: 1, b: 'a'}];

sand.find(array, {b: 'a'});       // {a: 1, b: 'a'}
sand.find(array, {b: 'k'});       // undefined
```

**Filter** - Creates a new array with all elements that pass the test
implemented by *callback*.

```javascript
var array = [{a: 2, b: 'b'}, {a: 1, b: 'a'}];

var callback = function (elem) {
    return elem.b === 'b';
};

sand.filter(array, callback);     // [{a: 2, b: 'b'}]
```

**Map** - Creates a new array with the results of calling the *callback* on
every element in this array.

```javascript
var array = [{a: 2, b: 'b'}, {a: 1, b: 'a'}];

var callback = function (elem) {
    return elem;
};

sand.map(array, callback);        // [{a: 2, b: 'b'}, {a: 1, b: 'a'}]
```

**Sort** - Sort an array according to informed key. Order default is *asc*.

```javascript
var array = [{a: 2, b: 'b'}, {a: 1, b: 'a'}];

sand.sort(array, 'a');            // [{a: 1, b: 'a'}, {a: 2, b: 'b'}]
sand.sort(array, 'b', 'asc');     // [{a: 1, b: 'a'}, {a: 2, b: 'b'}]
sand.sort(array, 'a', 'desc');    // [{a: 2, b: 'b'}, {a: 1, b: 'a'}]
sand.sort(array, 'b', 'desc');    // [{a: 2, b: 'b'}, {a: 1, b: 'a'}]
```

**Inject** - Inject methods for manipulation. It does **not** inject methods in
the **prototype**, it injects methods in the given array.

| Method        | Description                                                               |
| :------------ | :------------------------------------------------------------------------ |
| get           | returns the object at the current position or in the given position       |
| set           | change the current position of the array and call get                     |
| previous      | returns the object in the previous position found or false if there is no |
| next          | returns the object in the next position found or false if there is no     |

example:

```javascript
var array = [{a: 2, b: 'b'}, {a: 1, b: 'a'}];

sand.inject(array);

array.get();                      // {a: 2, b: 'b'}
array.get(1);                     // {a: 1, b: 'a'}

array.set({a: 1});                // {a: 1, b: 'a'}

array.get();                      // {a: 1, b: 'a'}
array.get(0);                     // {a: 2, b: 'b'}

array.previous();                 // {a: 2, b: 'b'}
array.previous();                 // false

array.next();                     // {a: 2, b: 'b'}
array.next();                     // {a: 1, b: 'a'}
array.next();                     // false
```

###### Object

**Has key** - Check if exists the key directly in object.

```javascript
var object = {a: 1, b: 2, c: 3, d: 4, e: 5};

sand.hasKey(object, 'd');         // true
sand.hasKey(object, 't');         // false
```

**Has value** - Check if exists the value directly in object.

```javascript
var object = {a: 1, b: 2, c: 3, d: 4, e: 5};

sand.hasValue(object, 4);         // true
sand.hasValue(object, 7);         // false
```

**Inject** - Inject methods for manipulation of keys. It does **not** inject
methods in the **prototype**, it injects methods in the given object.

| Method        | Description                                                            |
| :------------ | :--------------------------------------------------------------------- |
| keys          | returns all keys of object                                             |
| getKey        | returns the key at the current position                                |
| setKey        | change the current position of the key and call getKey                 |
| previousKey   | returns the key in the previous position found or false if there is no |
| nextKey       | returns the key in the next position found or false if there is no     |

example:

```javascript
var object = {a: 'a', b: 'b', c: 'c', d: 'd', e: 'e'};

sand.inject(object);

object.keys();                    // ['a', 'b', 'c', 'd', 'e']

object.getKey();                  // 'a'
object.setKey('c');               // 'c'

object.nextKey();                 // 'd'
object.nextKey();                 // 'e'

object.previousKey();             // 'd'
object.previousKey();             // 'c'
```

###### Date

**Now** - Gets the current timestamp as integer.

```javascript
sand.now();                       // example: 1432395347945
```

###### Number

**Is number** - Check if a value is number.

```javascript
sand.isNumber('test');            // false
sand.isNumber('');                // false
sand.isNumber(0);                 // true
sand.isNumber(7);                 // true
sand.isNumber('9');               // true
```

## Issues

For questions, bug reports, improve documentation, and feature request please
search through existing
[issue](https://github.com/hpedrorodrigues/sand/issues) and if you don't
find and answer open a new one [here](https://github.com/hpedrorodrigues/sand/issues/new).
If you need support send me an [email](mailto:hs.pedro.rodrigues@gmail.com). You can also
contact [me](https://github.com/hpedrorodrigues) for any non public concerns.

## Contribution

When issuing a pull request, please exclude changes in the **dist** folder to
avoid merge conflicts.

## License

Sand is released under the MIT license. See [LICENSE](./LICENSE) for details.

## More

Sand is a work in progress, feel free to improve it.

## References
- [AMD - CommonJS - UMD](http://davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/)
- [UMD Pattern](http://addyosmani.com/writing-modular-js/)
- [Throttle](https://msdn.microsoft.com/en-us/library/dn589798.aspx) -
  [Debounce](http://scn.sap.com/community/labs/blog/2014/04/30/debounce-and-throttle-in-javascript)
- [Loop Infinito - Throttle e Debounce patterns](http://loopinfinito.com.br/2013/09/24/throttle-e-debounce-patterns-em-javascript/)
- [Almir Filho - Throttle e Debounce patterns in web apps](https://speakerdeck.com/almirfilho/throttle-and-debounce-patterns-in-web-apps)
- [NPM](https://docs.npmjs.com/getting-started/publishing-npm-packages) -
  [Bower](http://bower.io/docs/creating-packages/)
- [A guide to publishing JavaScript open source projects](http://www.conductor.com/nightlight/guide-publishing-javascript-open-source-projects/)
