'use strict';

module.exports = function(karma) {
    karma.set({
        basePath: '',
        frameworks: ['browserify', 'jasmine'],
        files: [
            {pattern: 'node_modules/**/*.js', included: false},
            {pattern: 'app/index.js', included: false},
            {pattern: 'tests/**/*.js', included: true}
        ],
        preprocessors: {
            'tests/**/*.js' : ['browserify']
        },
        reporters: ['dots', 'junit'],
        junitReporter: {
            outputFile: 'reports/karma_test_results.xml'
        },
        port: 8080,
        colors: true,
        captureTimeout: 60000,
        logLevel: karma.LOG_INFO,
        autoWatch: false,
        singleRun: true,
        browserify: {
            debug: true,
            transform: ['brfs', 'browserify-shim']
        },
        browsers: ['PhantomJS']
    });
};