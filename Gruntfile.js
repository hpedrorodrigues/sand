'use strict';

module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: true,
                reporter: 'checkstyle',
                reporterOutput: 'reports/jshint-result.xml'
            },
            files: ['Gruntfile.js', 'app/*.js']
        },
        karma: {
            development: {
                configFile: 'karma.conf.js'
            }
        },
        uglify: {
            options: {
                compress: {
                    drop_console: true,
                    dead_code: true
                }
            },
            default: {
                files: {
                    'dist/sand.min.js': ['app/sand.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('default', ['jshint', 'uglify']);
};