'use strict';

function Sand() {

  var sand = this;
  var throttleTimeout = null;
  var debounceTimeout = null;

  sand.debounce = function (delay, callback) {
    return function () {
      var later = function() {
        callback.apply(this, arguments);
      };

      clearTimeout(debounceTimeout);
      debounceTimeout = setTimeout(later, delay);
    };
  };

  sand.throttle = function (delay, callback) {
    var context, args, result, lastExecution;

    function later() {
      lastExecution = sand.now();
      throttleTimeout = null;
      result = callback.apply(context, args);
      if (!throttleTimeout) {
        context = args = null;
      }
    }

    function clearDelay() {
      if (throttleTimeout) {
        clearTimeout(throttleTimeout);
        throttleTimeout = null;
      }
    }

    return function () {
      var now = sand.now();

      if (!lastExecution) {
        lastExecution = now;
      }

      var remaining = delay - (now - lastExecution);
      context = this;
      args = arguments;

      clearDelay();

      if (remaining <= 0 || remaining > delay) {
        lastExecution = now;
        result = callback.apply(context, args);

        if (!throttleTimeout) {
          context = args = null;
        }
      } else if (!throttleTimeout) {
        throttleTimeout = setTimeout(later, remaining);
      }

      return result;
    };
  };

  sand.now = function () {
    return Date.now() || new Date().getTime();
  };

  sand.hasKey = function (object, key) {
    var filtered = Object
      .keys(object)
      .filter(function(prop) {
        return prop === key;
      });
    return !!(filtered && filtered.length);
  };

  sand.hasValue = function (object, value) {
    var filtered = Object
      .keys(object)
      .filter(function(prop) {
        return object[prop] === value;
      });
    return !!(filtered && filtered.length);
  };

  sand.isNumber = function(value) {
    return !isNaN(parseFloat(value));
  };

  sand.sort = function (array, key, order) {
    return array.sort(function (previous, next) {
      var result = 0, previousKey = previous[key], nextKey = next[key];
      if (previousKey !== nextKey) {
        if (order === 'desc') {
          result = previousKey < nextKey ? 1 : -1;
        } else {
          result = previousKey > nextKey ? 1 : -1;
        }
      }
      return result;
    });
  };

  sand.find = function (array, rule) {
    sand.inject(rule);
    var key = rule.getKey();
    var found;

    array.some(function (elem) {
      if (elem[key] === rule[key]) {
        found = elem;
        return true;
      }
    });
    return found;
  };

  sand.filter = function (array, callback) {
    if (array && Array.isArray(array)) {
      return array.filter(callback);
    }

    return undefined;
  };

  sand.map = function (array, callback) {
    if (array && Array.isArray(array)) {
      return array.map(callback);
    }

    return undefined;
  };

  sand.inject = function (object) {
    if (Array.isArray(object)) {
      var array = object;
      var position = 0;

      array.get = function (index) {
        if (index && sand.isNumber(index)) {
          return this[Number(index)];
        }

        return this[position];
      };

      array.set = function (object) {
        if (sand.isNumber(object)) {
          position = Number(object);
          return this[position];
        }
        var current = sand.find(array, object);
        position = current ? array.indexOf(current) : position;

        return current;
      };

      array.previous = function () {
        if (position >= 0) {
          position--;
          return position < 0 ? false : this[position];
        }
        return false;
      };

      array.next = function () {
        if (position < this.length) {
          position++;
          return position === this.length ? false : this[position];
        }
        return false;
      };
    } else if (object && object !== null && typeof object === 'object') {
      var index = 0;

      object.keys = function () {
        var hidedKeys = {keys: true, getKey: true, setKey: true, previousKey: true, nextKey: true};

        return Object
          .keys(this)
          .filter(function (key) {
            return !hidedKeys[key];
          });
      };

      object.getKey = function () {
        return object.keys()[index];
      };

      object.setKey = function (currentKey) {
        index = object.keys().indexOf(currentKey);
        return object.getKey();
      };

      object.previousKey = function () {
        var previousIndex = --index;
        if(index === 0){
          return false;
        }
        return object.keys()[previousIndex];
      };

      object.nextKey = function () {
        var nextIndex = ++index;
        if(nextIndex >= object.keys().length){
          return false;
        }
        return object.keys()[nextIndex];
      };
    } else {
      throw new ReferenceError('Inject only be must used in "Objects" or "Arrays".');
    }
  };
}


if (typeof define === 'function' && define.amd) {
  define([], new Sand());
} else if (typeof exports === 'object') {
  module.exports = new Sand();
} else {
  var sand = new Sand();
}