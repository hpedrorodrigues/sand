require 'json'

def get_version
  file = File.read('../package.json')
  json = JSON.parse(file)
  json['version']
end

def increment_version (version, increment)
  types = {"major" => 0, "minor" => 1, "patch" => 2}
  version_parts = version.split(".")
  type_value = types[increment]
  version_parts[type_value] = version_parts[type_value].to_i() + 1
  new_type_value = type_value + 1
  (new_type_value ... version_parts.size).each{|i| version_parts[i] = 0}
  version_parts.join(".")
end

def write_version (version)
  file = File.read('../package.json')
  json = JSON.parse(file)
  json['version'] = version

  File.open("../package.json","w") {|f| f.write(JSON.pretty_generate(json))}
end

def main
  increment = ARGV[0]
  version = get_version
  new_version = increment_version(version, increment)
  write_version(new_version)
  puts new_version
end

main