#!/usr/bin/env bash

if [[ -z "$1" || ( "$1" != "patch" && "$1" != "minor" && "$1" != "major" ) ]]
then
  echo "Please call this with one of the following version options: patch, minor, major. :)"
  exit 1
fi

# STATUS=$(git status)
# if [[ $STATUS != *"nothing to commit"* || $STATUS != *"Your branch is up-to-date"* ]]
# then
#  echo "You must have a branch that is up to date to run this publisher. Please commit all changes and try again."
#  exit 1
# fi

VERSION=$(ruby increment-version.rb $1)
echo "Upgrading to version $VERSION :)"

grunt

git add .
git commit -a -m "updated to version $VERSION"
git push origin master

git tag -a $VERSION -m "released $VERSION"
git push origin $VERSION

npm publish ../
# exit 0